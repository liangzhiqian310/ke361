<?php
namespace Home\Model;
use Think\Model;
class CategoryGoodsModel extends Model{
   
    
    public function delCategory($category_id){
        $this->delete($category_id);
    }
    
    public function getGoodsCategory($state=1){
        $where = "type=1 "; 
        $where .= $state?" and status='1'":"1";
        return $this->where($where)->select();
    }
     
    public function getName($category_id){
        $category = $this->find($category_id);
        return $category['category_name'];
    }
}