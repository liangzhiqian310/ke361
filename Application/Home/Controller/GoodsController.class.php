<?php
namespace Home\Controller;
use Home\Model\GoodsModel;
use Home\Model\CategoryGoodsModel;
class GoodsController extends HomeController{
    public function index(){
      
        $where['status'] = 1;
        $GoodsModel = new GoodsModel();
        
        $goods = $this->lists($GoodsModel,$where);
        $this->assign('goods',$goods);
        $this->assign('totalPage',$totalPage);
        
        $this->setSiteTitle('商品');
        $this->display();
    }
    public function cate(){
        $where['id'] = I('id');
        $CategoryModel = new CategoryGoodsModel();
      
        $cate = $CategoryModel->where($where)->find();
       
        if(!$cate) $this->error ('栏目不存在');
        $goodsdb = M('goods');
        unset($where);
        $where['cate_id'] = I('get.id','','intval');
        $where['status'] = 1;
        $GoodsModel = new GoodsModel();
        
        $goods = $this->lists($GoodsModel,$where);
        $this->assign('goods',$goods);
        $this->assign('totalPage',$totalPage);
        $this->assign('cate',$cate);
        $this->setSiteTitle($cate['category_name']);
        $this->display();
    }
    
    public function info(){
        $id = I('get.id','','intval');
       
        
        $goods = D('Goods')->info($id);
        if($goods){
            M('goods')->where('id='.$id)->setInc('hits');
           
            $hot_goods = D('goods')->hotGoods($goods['cate_id']);
            $this->assign('hot_goods',$hot_goods);
            $this->assign('goods',$goods);
            $this->setSiteTitle($goods['title']);
            $this->display();   
        }else{
            $this->error('商品不存在');
        }
    }
    public function buttonInfo(){
        $id = I('post.id','','intval');
        if($id > 0){
            
            $info = D('Goods')->info($id);
            if($info['discount_price']>0){
                $span = "<span class='price'>".$info['discount_price']."</span";
            }else{
                $span = "<span class='price'>".$info['price']."</span";
            }
            $img = "<img src='".get_image_url($info['pic_url'])."' alt='".$info['title']."'>";
            
            $a    = "<a class='btn btn-goods-info' href='".U('Goods/info?id='.$id)."'><span>查看详情</span></a>"; 
            $div  = "<div class='goods-tap'>".$img.$span.$a."</div>";
            if($info){
                $result['status'] = 1;
                $result['src'] = get_image_url($info['pic_url']);
                $result['price'] = $info['discount_price'] > 0  ? $info['discount_price'] : $info['price'];
                $result['url']   = U('Goods/info?id='.$info['id']);
                $this->ajaxReturn($result);
            }
           
        }else {
            $result['status'] = 1;
            $result['content'] = '数据错误';
            $this->ajaxReturn($result);
        }
    }
    public function ajGetGoodsDetial(){
       
            $url = I('url');
            $data = file_get_contents($url);
            preg_match('/dsc\.taobaocdn\.com[\w\.\%\/]*[\"]/i', $data,$descurl);
            $descurl = "http://".trim($descurl[0],'"');
            $content = file_get_contents($descurl);
            
            if($content){
                $content = mb_convert_encoding($content, 'utf-8', 'GBK,UTF-8,ASCII');
                $content = trim($content,"var desc='");
                $where['item_url'] = htmlspecialchars_decode($url);
                $save['item_body'] = $content;
                $save['item_body'] = substr($save['item_body'],0,-3);
                M('goods')->where($where)->save($save);
                $result['status']= 1;
                $result['content'] = $content;
                $this->ajaxReturn($result);
            }else{
               
                $this->ajaxReturn("商品详情获取失败，请刷新重试");
            }
            
        
    }
    public function tag($id){
        if(!is_numeric($id) || $id <= 0){
            $this->error('非法的标签');
        }
         
         
        $where['status'] = 1;
        $where['tag']    = $id;
        $info = D('Tag')->info($id);
        $list = $this->lists(D('Goods'),$where);
      /*  if(empty($list)){
            redirect(U('Artircle/tag',array('id'=>$id)));
        }
      */
        $this->assign('goods',$list);
        $this->setSiteTitle($info['tag_name']);
        $this->display();
    }
}