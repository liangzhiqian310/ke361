<?php
namespace Home\Controller;
use Home\Model\GoodsModel;
class SearchController extends HomeController{
    public function __construct() {
        parent::__construct();
    }
    public function index(){
        $keywords = I('post.keywords') ? I('post.keywords'):I('get.keywords');
        if(empty($keywords)){
            $this->error ('请输入搜索的关键词');
        }
        $page = I('get.p','','intval');
        $where['title'] = array('like',"%".$keywords."%");
        $GoodsModel = new GoodsModel();
     
        
        $goods = $this->lists($GoodsModel,$where);
        $this->assign('page',$page+1);
        $this->assign('goods',$goods);
        $this->assign('keyword',$keywords);
  
        if(IS_AJAX){
            $this->display('ajgetgoods');
        }else{
           $this->display(); 
        }
        
    }
}